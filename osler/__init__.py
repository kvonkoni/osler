#!/usr/bin/env python

__all__ = ['assertion','common','criterion','diagnosis','issue']

import logging
import sys

logging.basicConfig(stream=sys.stdout, level=logging.INFO)
